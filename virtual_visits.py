#!/usr/bin/env python3

from __future__ import annotations
import json
import time
import urllib

import lxml.html

def eventIDs(category_id: int = 5975) -> list[str]:
    url = f'https://indico.cern.ch/export/categ/{category_id}.json'
    return [int(event.get('id')) for event in json.loads(urllib.request.urlopen(url).read()).get('results')]

def guidesURL(event_id: int = 855125) -> str:
    url = f'https://indico.cern.ch/event/{event_id}'
    event_html = lxml.html.fromstring(urllib.request.urlopen(url).read())
    guides_url = [e.attrib.get('href') for e in event_html.xpath('//li[@class="menuConfTitle"]/a') if 'guide' in e.text.lower()]
    if guides_url:
        return guides_url[0]

def eventID_ifGuide(guides_url: str = '/event/855125/page/18585-guides', name: str = 'delannoy') -> int:
    url = f'https://indico.cern.ch{guides_url}'
    guides_html = lxml.html.fromstring(urllib.request.urlopen(url).read())
    if name in guides_html.xpath('//div[@class="page-content"]')[0].text_content().lower():
        return int(guides_url.split('/')[2])

def main() -> list[str]:
    ids = eventIDs(category_id=5975) # this will get the ID for all indico events in the Virtual Visits category (https://indico.cern.ch/category/5975/)
    events = list()
    for idx, id in enumerate(ids): # loop over each VV indico event
        time.sleep(1)
        print(f'{idx}/{len(ids)}')
        guides_url = guidesURL(event_id=id) # identify the url for the guides subpage (this changes for every event and I couldn't figure out how to obtain it through the API)
        if guides_url:
            event_id = eventID_ifGuide(guides_url=guides_url, name='delannoy') # return the VV indico event ID if `name` is in the body of the guides subpage.
            if event_id:
                print(f'https://indico.cern.ch/event/{event_id}')
                events.append(event_id)
    return [f'https://indico.cern.ch/event/{id}' for id in sorted(events)]
