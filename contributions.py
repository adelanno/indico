#!/usr/bin/env python3

from __future__ import annotations
import json
import os
import typing
import urllib.parse
import urllib.request

import pandas

'''https://docs.getindico.io/en/latest/http-api/'''

API_TOKEN = os.getenv('INDICO_API_TOKEN') # https://indico.cern.ch/user/tokens/
HEADERS = {'Authorization': f'Bearer {API_TOKEN}'}

def getItem(url: str) -> list[dict[str, typing.Any]]:
    '''Generic GET request function.'''
    request = urllib.request.Request(method='GET', url=url, headers=HEADERS)
    response = urllib.request.urlopen(url=request)
    return json.loads(response.read().decode('utf-8')).get('results')

def getCategory(category_id: int, year: int) -> list[dict[str, typing.Any]]:
    '''Query an Indico category for a given year.'''
    params = urllib.parse.urlencode(query={'from': f'{year}-01-01', 'to': f'{year}-12-31'})
    url = f'https://indico.cern.ch/export/categ/{category_id}.json?{params}'
    return getItem(url=url)

def getEvent(event_id: int) -> dict[str, typing.Any]:
    '''Query an Indico event.'''
    params = urllib.parse.urlencode(query={'detail': 'sessions'})
    url = f'https://indico.cern.ch/export/event/{event_id}.json?{params}'
    event = getItem(url=url)
    if len(event) != 1:
        raise ValueError('More than one event returned')
    return event[0]

def getContribution(contribution: dict[str, typing.Any], session_title: str = '') -> dict[str, typing.Any]:
    '''Process Indico contribution.'''
    id = contribution.get('db_id')
    title = f"{session_title} | {contribution.get('title')}" if session_title else contribution.get('title')
    date = f"{contribution.get('startDate').get('date')} {contribution.get('startDate').get('time')}"
    speakers = [f"{speaker.get('first_name')} {speaker.get('last_name')}" for speaker in contribution.get('speakers')]
    speakers_email = [speaker.get('email') for speaker in contribution.get('speakers')]
    download_url = [attachment.get('download_url') for folder in contribution['folders'] for attachment in folder['attachments']]
    link_url = [attachment.get('link_url') for folder in contribution['folders'] for attachment in folder['attachments']]
    return {'date': date, 'title': title, 'speakers': speakers, 'speakers_email': speakers_email, 'download_url': download_url}

def getAllContributions(event: dict[str, typing.Any]) -> pandas.DataFrame:
    '''Process all contributions in an Indico event.'''
    contributions = [getContribution(contribution) for contribution in event.get('contributions', [])]
    contributions += [getContribution(contribution) for contribution in event.get('subcontributions', [])]
    for session in event.get('sessions'):
        contributions += [getContribution(contribution, session.get('title')) for contribution in session.get('contributions', [])]
        contributions += [getContribution(contribution, session.get('title')) for contribution in session.get('subcontributions', [])]
    return pandas.DataFrame(contributions)

def main(category_name: str, category_id: int, year: int) -> None:
    '''Export info for all contributions in a given category for a given year.'''
    event_ids = [int(event.get('id')) for event in getCategory(category_id=category_id, year=year)]
    events = [getEvent(event_id) for event_id in event_ids]
    contributions = pandas.concat([getAllContributions(event) for event in events], axis=0).reset_index(drop=True)
    contributions = contributions.explode(['speakers', 'speakers_email']).explode('download_url')
    contributions.to_csv(f'{category_name}_contributions.csv', index=False)